# How to Use ?

1- Adicione as classes do SBA no seu persistence.xml: 
   ``<class>br.com.totvs.summer.bank.abrangencia.bean.SbaUnidade</class>
      <class>br.com.totvs.summer.bank.abrangencia.bean.SbaUnidadeNotExist</class>``
      
2- Se o Qualifier onde estão localizadas suas classes não for o Default.class, você deve apontar o qualifier no web.xml, 
segue exemplo:
``<context-param>
        <param-name>br.com.totvs.summer.bank.abrangencia.qualifierClass</param-name>
        <param-value>br.com.totvs.summer.persistence.annotation.Base1</param-value>
    </context-param>``
    
3- Não esqueça de criar as tabelas referentes ao SbaUnidade e SbaUnidadeNotExist