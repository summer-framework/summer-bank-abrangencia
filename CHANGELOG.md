# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 1.3.2.2018062815
### Changed
- Atualização do summer-web

## 1.3.1.2018061214
### Changed
- Atualização do summer-web
- Adição do equals() e hashCode() na classe SbaUnidadeNotExistPK

## 1.3.0.2018052910
### Added
- Com o intuito de adicionar a possibilidade de remover algumas unidades de acordo com o módulo, foi adicionada a classe SbaUnidadeNotExist. 
Seu objetivo é conter todas as unidades que não devem ser apresentadas em determinado módulo, por isso sua chave é: cdUnd + sgMdl. 
- Obrigatoriamente deve ser adicionado ao arquivo persistence.xml do seu projeto a classe SbaUnidadeNotExist, no mesmo persistenceUnit do já adicionado SbaUnidade.
- Inclusão da coluna idSit na entidade SbaUnidade, para que seja possível retornar apenas as Unidades com situação Ativa.


Obs: deverá ser meapeado no persistence.xml do commons do projeto a entidade: br.com.totvs.summer.bank.abrangencia.bean.SbaUnidadeNotExist no basicoDS

## 1.2.0.2018052211
### Added
- Criação da página sba_box_abrangencia_nowell, usado para casos em que o '<div class=well>' não deve ser inserido

## 1.1.1.2018052109
### Changed 
 - Atualização do summer-web

## 1.1.0.2018051015
### Added
- Inclusão da regra para quando o usuário logado possuir acesso apenas a uma unidade, a mesma já venha selecionada apenas para visualização.
- Inclusão da regra para quando o usuário logado possuir acesso apenas a uma unidade não será disponibilizado a opção do usuário "trocar abrangência".

## 1.0.16.2018051014
### Changed 
 - Atualização do summer-web

## 1.0.15.2018050711
### Changed 
 - Atualização do summer-web
 - Atualização do Servlet API para 3.1.0 afim de seguir o JEE7

## 1.0.14.2018050315
### Changed 
 - Atualização do summer-web

## 1.0.13.2018042310
### Changed 
 - Atualização do summer-web

## 1.0.12.2018041811
### Changed 
 - Atualização do summer-web

## 1.0.11.2018041713
### Changed 
 - Atualização do summer-web

## ver 1.0.10.2018041114
- Atualização do summer-web

## ver 1.0.9.2018041014
- Atualização do summer-web

## ver 1.0.8.2018040517
- Atualização do summer-web

## ver 1.0.7.2018040210
- Atualização do summer-web

## ver 1.0.6.2018032613
- Atualização do summer-web

## ver 1.0.5.2018032313
- Atualização do summer-web

## ver 1.0.4.2018031311
- Atualização do summer-web

## ver 1.0.3.2018031217
- Atualização do summer-bank-abrangencia

## ver 1.0.2.2018022614
- Criação do atributo cdUnd na classe SbaSessionParam para que retorne o código da unidade selecionada.

## ver 1.0.1.2018021910
- Atualização do summer-web

## ver 1.0.0.2018021511
- Versão Inicial