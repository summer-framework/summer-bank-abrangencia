package br.com.totvs.summer.bank.abrangencia.dto;

import br.com.totvs.summer.core.util.StringUtils;

import java.io.Serializable;
import java.util.Objects;

/**
 * This class work as a container to store the values selected in XHTML page
 */
public class SbaSessionParam implements Serializable {
    private String empresa;
    private String unidade;
    private Integer cdUnd;
    private String posto;

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getPosto() {
        return posto;
    }

    public void setPosto(String posto) {
        this.posto = posto;
    }

    public Integer getCdUnd() {
        if (StringUtils.isEmpty(unidade)) {
            return null;
        } else {
            return Integer.valueOf(unidade.split("-")[0]);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SbaSessionParam that = (SbaSessionParam) o;
        return cdUnd == that.cdUnd &&
                Objects.equals(empresa, that.empresa) &&
                Objects.equals(unidade, that.unidade) &&
                Objects.equals(posto, that.posto);
    }

    @Override
    public int hashCode() {

        return Objects.hash(empresa, unidade, cdUnd, posto);
    }
}
