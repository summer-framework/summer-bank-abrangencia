package br.com.totvs.summer.bank.abrangencia.filter;

import br.com.totvs.summer.bank.abrangencia.bean.SbaUnidade;
import br.com.totvs.summer.bank.abrangencia.dto.SbaSessionParam;
import br.com.totvs.summer.bank.abrangencia.util.SbaUtil;
import br.com.totvs.summer.security.dto.Subject;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class SbaFilter implements Filter {

    public void destroy() {

    }

    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        HttpSession sess = ((HttpServletRequest) request).getSession(false);
        String servletPath = ((HttpServletRequest) request).getServletPath();

        if (sess != null) {
            sess.setAttribute(SbaUtil.SBA_OLD_URI, servletPath);
            Subject subject = (Subject) sess.getAttribute("subject");
            if (subject != null) {
                SbaSessionParam abrangencia = (SbaSessionParam) subject.getCustomValues().get(SbaUtil.UNIDADE_SESSION_PARAM_NAME);
                if (abrangencia == null) {
                    String contextPath = ((HttpServletRequest) request)
                            .getContextPath();
                    ((HttpServletResponse) response).sendRedirect(contextPath
                            + "/sba_select_page.xhtml");
                }
            }
        }

        chain.doFilter(request, response);
    }

    public void init(FilterConfig arg0) throws ServletException {

    }

}
