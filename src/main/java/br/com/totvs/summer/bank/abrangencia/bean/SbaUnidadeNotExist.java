package br.com.totvs.summer.bank.abrangencia.bean;

import br.com.totvs.summer.bank.abrangencia.bean.pk.SbaUnidadeNotExistPK;
import br.com.totvs.summer.persistence.bean.AbstractEntityAtu;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SBA_UNIDADE_NOTEXIST")
public class SbaUnidadeNotExist extends AbstractEntityAtu {

    private static final long serialVersionUID = -4933536842692151833L;

    @EmbeddedId
    private SbaUnidadeNotExistPK id;

    @Override
    public SbaUnidadeNotExistPK getId() {
        return id;
    }

    public void setId(SbaUnidadeNotExistPK id) {
        this.id = id;
    }


}
