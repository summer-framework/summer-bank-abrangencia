package br.com.totvs.summer.bank.abrangencia.dataquery;

public class SbaUnidadeDataQuery {

    public static final String FINDBY_CDUNDFIS = "SELECT u " +
            "FROM SbaUnidade u WHERE u.cdUndFis = :cdUnd AND u.idSit = 'A' AND u.cdUndFis not in (SELECT un.id.cdUnd FROM SbaUnidadeNotExist un WHERE un.id.siglaModulo = :sgMdl)";
    public static final String FINDBY_CDSUP = "SELECT u " +
            "FROM SbaUnidade u WHERE u.cdSup = :cdSup AND u.idSit = 'A' AND u.cdUndFis not in (SELECT un.id.cdUnd FROM SbaUnidadeNotExist un WHERE un.id.siglaModulo = :sgMdl) ORDER BY u.cdUndFis";

}
