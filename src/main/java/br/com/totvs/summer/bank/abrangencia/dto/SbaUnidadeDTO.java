package br.com.totvs.summer.bank.abrangencia.dto;

import br.com.totvs.summer.bank.abrangencia.bean.SbaUnidade;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SbaUnidadeDTO implements Serializable {

    private SbaUnidade unidade;
    private List<SbaUnidadeDTO> childs;

    public SbaUnidadeDTO() {
        this.childs = new ArrayList<>();
    }

    public SbaUnidade getUnidade() {
        return unidade;
    }

    public void setUnidade(SbaUnidade unidade) {
        this.unidade = unidade;
    }

    public List<SbaUnidadeDTO> getChilds() {
        return childs;
    }

    public void setChilds(List<SbaUnidadeDTO> childs) {
        this.childs = childs;
    }
}
