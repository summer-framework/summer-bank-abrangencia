package br.com.totvs.summer.bank.abrangencia.mb;

import br.com.totvs.summer.bank.abrangencia.dto.SbaSessionParam;
import br.com.totvs.summer.bank.abrangencia.util.SbaUtil;
import br.com.totvs.summer.security.dto.Subject;
import br.com.totvs.summer.web.mb.AbstractMB;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named
@SessionScoped
public class UtilSbaMB extends AbstractMB {
    public SbaSessionParam getSbaSessionParam(){
        Subject subject = getSubject();
        return (SbaSessionParam) subject.getCustomValues().get(SbaUtil.UNIDADE_SESSION_PARAM_NAME);
    }

    public String changeAbrangencia(){
        return navigateTo("/sba_select_page.xhtml");
    }
}
