package br.com.totvs.summer.bank.abrangencia.service;

import br.com.totvs.summer.bank.abrangencia.bean.SbaUnidade;
import br.com.totvs.summer.bank.abrangencia.dataquery.SbaUnidadeDataQuery;
import br.com.totvs.summer.bank.abrangencia.dto.SbaUnidadeDTO;
import br.com.totvs.summer.bank.abrangencia.util.SbaUtil;
import br.com.totvs.summer.core.dto.NamedParams;
import br.com.totvs.summer.core.exception.ServiceException;
import br.com.totvs.summer.core.service.GenericService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.log4j.Logger;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

@Named
@RequestScoped
public class SbaUnidadeService extends GenericService {

    public SbaUnidadeDTO findUnidadesBySup(Integer cdUndSup, String sgMdl,
                                           Class qualifier) throws ServiceException {
        List result = this.findList(SbaUnidadeDataQuery.FINDBY_CDUNDFIS,
                new NamedParams("cdUnd", cdUndSup, "sgMdl", sgMdl), qualifier, true);
        if (CollectionUtils.isEmpty(result)) {
            throw new ServiceException("Não foi encontrada nenhuma Unidade com o número " + cdUndSup);
        }
        SbaUnidade unidade = (SbaUnidade) result.get(0);
        return getUnidadesBySUP(unidade, sgMdl, qualifier);
    }

    private SbaUnidadeDTO getUnidadesBySUP(SbaUnidade unidade, String sgMdl, Class qualifier) throws ServiceException {
        SbaUnidadeDTO dto = new SbaUnidadeDTO();
        dto.setUnidade(unidade);
        List<SbaUnidade> unidadeList = null;
        unidadeList = this.findList(SbaUnidadeDataQuery.FINDBY_CDSUP,
                new NamedParams("cdSup", unidade.getCdUndFis(), "sgMdl", sgMdl), qualifier, true);
        for (SbaUnidade uo : unidadeList) {
            dto.getChilds().add(getUnidadesBySUP(uo, sgMdl, qualifier));
        }

        return dto;
    }
}
