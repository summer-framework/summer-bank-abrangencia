package br.com.totvs.summer.bank.abrangencia.bean.pk;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class SbaUnidadeNotExistPK implements Serializable {

    @Column(name = "cd_und")
    private Integer cdUnd;

    @Column(name = "sg_mdl")
    private String siglaModulo;

    public Integer getCdUnd() {
        return cdUnd;
    }

    public void setCdUnd(Integer cdUnd) {
        this.cdUnd = cdUnd;
    }

    public String getSiglaModulo() {
        return siglaModulo;
    }

    public void setSiglaModulo(String siglaModulo) {
        this.siglaModulo = siglaModulo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SbaUnidadeNotExistPK that = (SbaUnidadeNotExistPK) o;
        return Objects.equals(cdUnd, that.cdUnd) &&
                Objects.equals(siglaModulo, that.siglaModulo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(cdUnd, siglaModulo);
    }

    @Override
    public String toString() {
        return "SbaUnidadeNotExistPK{" +
                "cdUnd=" + cdUnd +
                ", siglaModulo='" + siglaModulo + '\'' +
                '}';
    }
}
