package br.com.totvs.summer.bank.abrangencia.bean;

import br.com.totvs.summer.persistence.bean.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "sba_unidade")
public class SbaUnidade extends AbstractEntity {
    @Id
    @Column(name = "cd_und_fis")
    private Integer cdUndFis;

    @Column(name = "cd_sup")
    private Integer cdSup;

    @Column(name = "nm_und")
    private String nmUnd;

    @Column(name = "id_sit")
    private String idSit;

    @Override
    public Object getId() {
        return cdUndFis;
    }

    public Integer getCdUndFis() {
        return cdUndFis;
    }

    public void setCdUndFis(Integer cdUndFis) {
        this.cdUndFis = cdUndFis;
    }

    public Integer getCdSup() {
        return cdSup;
    }

    public void setCdSup(Integer cdSup) {
        this.cdSup = cdSup;
    }

    public String getNmUnd() {
        return nmUnd;
    }

    public void setNmUnd(String nmUnd) {
        this.nmUnd = nmUnd;
    }

    public String getIdSit() {return idSit;}

    public void setIdSit(String idSit) {this.idSit = idSit;}
}
