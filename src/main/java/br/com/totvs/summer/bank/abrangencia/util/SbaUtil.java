package br.com.totvs.summer.bank.abrangencia.util;

import br.com.totvs.summer.bank.abrangencia.dto.SbaUnidadeDTO;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public abstract class SbaUtil implements Serializable {
    public static final String UNIDADE_SESSION_PARAM_NAME = "SBA_UNIDADE";
    public static final String SBA_OLD_URI = "SBA_OLD_URI";
    public static final String ESPACE_HTML = "&nbsp;";

    public static List<String> convertUnidadeOperacionalDTOToString(SbaUnidadeDTO dto) {
        return convertUnidadeOperacionalDTOToString(dto, 0);
    }

    private static List<String> convertUnidadeOperacionalDTOToString(SbaUnidadeDTO dto, Integer spaces) {
        List<String> unidadesAsStr = new ArrayList<>();
        StringBuilder strSpaces = new StringBuilder();
        for (int i = 0; i < spaces; i++) {
            strSpaces.append(SbaUtil.ESPACE_HTML + SbaUtil.ESPACE_HTML + SbaUtil.ESPACE_HTML);
        }

        unidadesAsStr.add(strSpaces.toString() + dto.getUnidade().getCdUndFis() + "-" + dto.getUnidade().getNmUnd());
        spaces++;
        for (SbaUnidadeDTO dtoInner : dto.getChilds()) {
            unidadesAsStr.addAll(convertUnidadeOperacionalDTOToString(dtoInner, spaces));
        }

        return unidadesAsStr;
    }
}
