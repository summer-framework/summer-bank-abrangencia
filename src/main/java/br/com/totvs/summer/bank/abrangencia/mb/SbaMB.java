package br.com.totvs.summer.bank.abrangencia.mb;

import br.com.totvs.summer.bank.abrangencia.dto.SbaSessionParam;
import br.com.totvs.summer.bank.abrangencia.service.SbaUnidadeService;
import br.com.totvs.summer.bank.abrangencia.util.SbaUtil;
import br.com.totvs.summer.core.exception.CoreException;
import br.com.totvs.summer.core.exception.ServiceException;
import br.com.totvs.summer.core.util.StringUtils;
import br.com.totvs.summer.security.dto.Subject;
import br.com.totvs.summer.web.mb.AbstractMB;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.Map;

@Named(value = "sbaMB")
@SessionScoped
public class SbaMB extends AbstractMB {

    @Inject
    private SbaUnidadeService unidadeService;

    private List<String> unidadesAsStr;
    private String unidade;

    @PostConstruct
    public void init() {
        Subject subject = getSubject();
        if (subject == null) {
            addErrorMessage("Objeto Subject vazio, não é possível carregar a abrangência corretamente");
            return;
        }

        if (subject.getCdLoc() == null) {
            addErrorMessage("O CdLoc do Usuário não pode ser vazio");
            return;
        }

        try {

            unidadesAsStr = SbaUtil.convertUnidadeOperacionalDTOToString(unidadeService.findUnidadesBySup(subject.getCdLoc(), getSiglaModulo(),
                    getQualifier()));

            if (subject.getCustomValues().containsKey(SbaUtil.UNIDADE_SESSION_PARAM_NAME)) {
                SbaSessionParam abrangenciaSessionParam = (SbaSessionParam) subject.getCustomValues().get(SbaUtil.UNIDADE_SESSION_PARAM_NAME);
                if (unidadesAsStr.contains(abrangenciaSessionParam.getUnidade())) {
                    this.unidade = abrangenciaSessionParam.getUnidade();
                }
            }else{
                if(unidadesAsStr.size()==1){
                    this.unidade = unidadesAsStr.get(0);
                }
            }
        } catch (Exception e) {
            addErrorMessage("Não foi possível carregar as unidades. " + e.getMessage());
        }
    }

    public String selecionarUnidade() {

        if (StringUtils.isEmpty(unidade)) {
            addErrorMessage("É necessário selecionar uma unidade para continuar");
            return "";
        }
        SbaSessionParam abrangenciaSessionParam = new SbaSessionParam();
        abrangenciaSessionParam.setEmpresa("1");
        abrangenciaSessionParam.setUnidade(unidade.trim().replace(SbaUtil.ESPACE_HTML, ""));
        Subject subject = getSubject();
        subject.getCustomValues().put(SbaUtil.UNIDADE_SESSION_PARAM_NAME, abrangenciaSessionParam);
        getContextoUtil().setParamSession("subject", subject);
        addInfoMessage("Unidade " + abrangenciaSessionParam.getUnidade() + " selecionada");

        Boolean redirectOldPage = Boolean.valueOf(getContextoUtil().getFacesContext().getExternalContext().getInitParameter("br.com.totvs.summer.bank.abrangencia.redirectOldPage"));
        if(redirectOldPage) {
            String url = this.navigateTo(this.getContextoUtil().getParamSession("SBA_OLD_URI").toString());
            String urlNew = url.replaceAll("([^/]+)$", "index.xhtml?faces-redirect=true");
            this.getContextoUtil().setParamSession("SBA_OLD_URI", urlNew);
            return urlNew;
        } else {
            return navigateTo(getContextoUtil().getParamSession(SbaUtil.SBA_OLD_URI).toString());
        }
    }

    private Class getQualifier() {
        String qualifierClass = getContextoUtil().getFacesContext().getExternalContext().getInitParameter("br.com.totvs.summer.bank.abrangencia.qualifierClass");
        if (StringUtils.isEmpty(qualifierClass)) {
            return Default.class;
        } else {
            try {
                return Class.forName(qualifierClass);
            } catch (ClassNotFoundException e) {
                addErrorMessage("A classe " + qualifierClass + " não foi encontrada. " + e.getMessage());
                return null;
            }
        }

    }

    public List<String> getUnidadesAsStr() {
        return unidadesAsStr;
    }

    public void setUnidadesAsStr(List<String> unidadesAsStr) {
        this.unidadesAsStr = unidadesAsStr;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }
}
